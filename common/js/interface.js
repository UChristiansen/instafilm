$(document).ready(function () {

    /* MAIN NAVIGATION TOGGLE BUTTON */
    $(".navbar-toggle").on("click", function () {
        $(this).toggleClass("active");
    });

    /* PROGRESS CONTROLLER */
    $("#progress-items a").on("click", function (e) {
        var currentAttrValue = $(this).attr("href");

        /* Show/Hide Tasks */
        $(".tasks " + currentAttrValue).show().siblings().hide();

        /* Change/remove current task to active */
        $(this).parent("li").addClass("active").siblings().removeClass("active");

        var progress = $(this).text();

        /* Change progress h3 to current task */
        $("#progress h3").html(progress);

        e.preventDefault();
    });

    $(".task-forward").on("click", function (e) {
        var nextAttrValue = $(this).attr("href");
        // var currentAttrValue = nextAttrValue

        /* Show/Hide Tasks */
        $(".tasks " + nextAttrValue).show().siblings().hide();

        /* Change/remove current task to active */
        $("#progress-items .active").next().addClass("active").siblings().removeClass("active");

        var progress = $("#progress-items .active").text();

        /* Change progress h3 to current task */
        $("#progress h3").html(progress);

        e.preventDefault();
    });

    $(".task-backward").on("click", function (e) {
        var prevAttrValue = $(this).attr("href");

        /* Show/Hide Tasks */
        $(".tasks " + prevAttrValue).show().siblings().hide();

        /* Change/remove current task to active */
        $("#progress-items .active").prev().addClass("active").siblings().removeClass("active");

        var progress = $("#progress-items .active").text();

        /* Change progress h3 to current task */
        $("#progress h3").html(progress);

        e.preventDefault();
    });

    /* PROGRESS TOGGLE BUTTON */
    var progressToggle = 0;
    $("#progress-toggle").click(function () {
        if (progressToggle == 0) {
            $("#progress-items").slideDown();
            $("#progress h3").fadeOut();
            $("#progress-toggle").html("<i class='fa fa-chevron-up' aria-hidden='true'></i>")
            progressToggle++;
        } else {
            $("#progress-items").slideUp();
            $("#progress h3").fadeIn();
            $("#progress-toggle").html("<i class='fa fa-chevron-down' aria-hidden='true'></i>")
            progressToggle--;
        }
    });

    /* CART TOGGLE BUTTON */
    var cartToggle = 0;
    $("#cart-toggle").click(function () {
        if (cartToggle == 0) {
            $("#cart-items").slideDown();
            $("#cart h3").fadeOut();
            $("#cart-toggle").html("<i class='fa fa-chevron-down' aria-hidden='true'></i>")
            cartToggle++;
        } else {
            $("#cart-items").slideUp();
            $("#cart h3").fadeIn();
            $("#cart-toggle").html("<i class='fa fa-chevron-up' aria-hidden='true'></i>")
            cartToggle--;
        }
    });

    /* PROGRESS & CART TOGGLE ON START */
    var documentWidth = $(document).width();
    if (documentWidth > 1200) {
        $("#progress-items").show();
        $("#cart-items").show();
        $("#cart h3").hide();
        $("#progress h3").hide();
    } else {
        $("#progress-items").hide();
        $("#cart-items").hide();
        $("#cart h3").show();
        $("#progress h3").show();
    }

    /* PRICING*/
    var priceFilm = 3999;
    var priceMusic = 0;
    var priceSubtitles = 0;

    /* ADDONS */
    /* MUSIC */
    $("input#addons_music").click(function () {
        if ($("input#addons_music").prop("checked")) {
            $("#cart-film").after("<tr id='cart-music'><td class='options'><button type='submit' class='delete'><i class='fa fa-times' aria-hidden='true'></i></button></td><td>Musik</td><td>999 DKK</td></tr>");
            priceMusic = 999;
            var priceTotal = priceFilm + priceMusic + priceSubtitles;
            $(".cart-total-price").html(priceTotal + " DKK");
        } else {
            $("#cart-music").remove();
            priceMusic = 0;
            var priceTotal = priceFilm + priceMusic + priceSubtitles;
            $(".cart-total-price").html(priceTotal + " DKK");
        }
    });

    /* SUBTITLES */
    $("input#addons_subtitles").click(function () {
        if ($("input#addons_subtitles").prop("checked")) {
            $("#cart-film").after("<tr id='cart-subtitles'><td class='options'><button type='submit' class='delete'><i class='fa fa-times' aria-hidden='true'></i></button></td><td>Undertekster</td><td>799 DKK</td></tr>");
            priceSubtitles = 799;
            var priceTotal = priceFilm + priceMusic + priceSubtitles;
            $(".cart-total-price").html(priceTotal + " DKK");
        } else {
            $("#cart-subtitles").remove();
            priceSubtitles = 0;
            var priceTotal = priceFilm + priceMusic + priceSubtitles;
            $(".cart-total-price").html(priceTotal + " DKK");
        }
    });

    /* ANIMATIONS */

    /* DELETE */
    $(".delete").click(function () {
        $(this).parent().parent().css("background-color", "red").fadeOut(1000, function () {
            $(this).remove();
        });
    });

    /* RESTORE */
    $(".restore").click(function () {
        $(this).parent().parent().css("background-color", "green").fadeOut(1000, function () {
            $(this).detach()
            $(".enabled").find('tbody').append(this).css("display", "").css("background-color", "");
        });
    });

});

/* PROGRESS & CART TOGGLE ON RESIZE */
$(window).resize(function () {
    var documentWidth = $(document).width();
    if (documentWidth > 1200) {
        $("#progress-items").show();
        $("#cart-items").show();
        $("#cart h3").hide();
        $("#progress h3").hide();
    } else {
        $("#progress-items").hide();
        $("#cart-items").hide();
        $("#cart h3").show();
        $("#progress h3").show();
    }
});

/* SHOW/HIDE NAVIGATION ON FRONTPAGE */
$(document).ready(function () {
    var documentWidth = $(document).width();
    if (documentWidth > 1200) {
        if ($(document).scrollTop() >= 50) {
            $('.navbar-frontpage').removeClass('top-scroll');
            $('.navbar-frontpage .logo').attr("src", "../common/media/logo_color.png");
        } else {
            $('.navbar-frontpage').addClass('top-scroll');
            $('.navbar-frontpage .logo').attr("src", "../common/media/logo_negative.png");
        }
    } else {
        $('.navbar-frontpage').removeClass('top-scroll');
        $('.navbar-frontpage .logo').attr("src", "../common/media/logo_color.png");
    }
});

/* BREAK NAVIGATION ON SCROLL */
$(document).scroll(function () {
    var documentWidth = $(document).width();
    if (documentWidth > 1200) {
        if ($(document).scrollTop() >= 50) {
            $('.navbar-frontpage').removeClass('top-scroll');
            $('.navbar-frontpage .logo').attr("src", "../common/media/logo_color.png");
        } else {
            $('.navbar-frontpage').addClass('top-scroll');
            $('.navbar-frontpage .logo').attr("src", "../common/media/logo_negative.png");
        }
    } else {
        $('.navbar-frontpage').removeClass('top-scroll');
        $('.navbar-frontpage .logo').attr("src", "../common/media/logo_color.png");
    }
});

/* BREAK NAVIGATION ON RESIZE */
$(window).resize(function () {
    var documentWidth = $(document).width();
    if (documentWidth > 1200) {
        if ($(document).scrollTop() >= 50) {
            $('.navbar-frontpage').removeClass('top-scroll');
            $('.navbar-frontpage .logo').attr("src", "../common/media/logo_color.png");
        } else {
            $('.navbar-frontpage').addClass('top-scroll');
            $('.navbar-frontpage .logo').attr("src", "../common/media/logo_negative.png");
        }
    } else {
        $('.navbar-frontpage').removeClass('top-scroll');
        $('.navbar-frontpage .logo').attr("src", "../common/media/logo_color.png");
    }
});

/* SOUND CONTROLS */
$(document).ready(function () {
    $(".play-sound").click(function () {
        if ($("video").prop('muted')) {
            $(".play-sound").html('<i class="fa fa-volume-off" aria-hidden="true"></i> Spil uden lyd');
            $("video").prop('muted', false);
        } else {
            $(".play-sound").html('<i class="fa fa-volume-up" aria-hidden="true"></i> Spil med lyd');
            $("video").prop('muted', true);
        }
    });
});



/***********************
        Validation
************************/


$().ready(function () {

    $.validator.setDefaults({ 
        errorClass: 'form-control-feedback',
        highlight: function (element) {
            $(element)
                .closest('.form-group')
                .addClass('has-error'); /* ADD ERROR CLASS */
        },
        unhighlight: function (element) {
            $(element)
                .closest('.form-group')
                .removeClass('has-error'); /* REMOVE ERROR CLASS */
        }

    });


    $("#createaccount").validate({ /* WHAT FORM TO VALIDATE */

        /* DEFINE VALIDATION RULES */
        rules: {
            navn: {
                required: true
            },
            brugernavn: {
                required: true,
                minlength: 4
            },
            password: {
                required: true,
                minlength: 5
            },
            passwordconf: {
                required: true,
                minlength: 5,
                equalTo: "#password"
            },
            email: {
                required: true,
                email: true
            },
            mobilnummer: {
                required: true,
                digits: true,
                minlength: 8,
                maxlength: 8
            }
        },

        /* DISPLAY ERROR MESSAGES */
        messages: {

            navn: {
                required: "Udfyld venligst dit navn"
            },
            brugernavn: {
                required: "Udfyld venligst dit brugernavn",
                minlength: "Dit brugernavn skal bestå af mindst 4 tegn"
            },
            password: {
                required: "Udfyld venligst dit password",
                minlength: "Dit password skal bestå af mindst 5 tegn"
            },
            passwordconf: {
                required: "Udfyld venligst dit password",
                minlength: "Dit password skal bestå af mindst 5 tegn",
                equalTo: "Password skal stemme overens"
            },
            email: {
                required: "Udfyld venligst din email",
                email: "Udfyld venligst en gyldig email"
            },
            mobilnummer: {
                required: "Udfyld venligst dit mobilnummer",
                digits: "Dit mobilnummer må kun indeholde tal",
                minlength: "Dit mobilnummer skal være på 8 cifrer",
                maxlength: "Dit mobilnummer skal være på 8 cifrer"
            }
        }




    });


});