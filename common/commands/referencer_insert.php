<?php
include "../commands/sqlconnect.php";

$showcaseID=$_POST['showcase_ID'];
$showcaseTitle=$_POST['showcase_title'];
$showcaseDescription=$_POST['showcase_description'];
$showcaseLink=$_POST['showcase_link'];
$showcaseUpload=$_POST['showcase_upload'];
$showcaseDisabled=$_POST['showcase_disabled'];
$administratorID=$_POST['administrator_ID'];

$insert = "INSERT INTO showcase
(
    showcase_title,
    showcase_description,
    showcase_link,
    showcase_upload,
    showcase_disabled,
    administrator_ID
)
VALUES
(
'" . $showcaseTitle . "', 
'" . $showcaseDescription . "', 
'" . $showcaseLink . "',  
'" . $showcaseUpload . "', 
'" . $showcaseDisabled . "', 
'" . $administratorID . "'
);
";

if ($mysqli->query($insert) === TRUE) {
    include "../commands/sqlclose.php";
    header("Location: ../../admin/?page=referencer");
} else {
    echo "Error updating record: " . $mysqli->error;
    include "../commands/sqlclose.php";
}

?>