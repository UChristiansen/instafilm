<?php
include "../commands/sqlconnect.php";

$showcaseID=$_POST['showcase_ID'];
$showcaseTitle=$_POST['showcase_title'];
$showcaseDescription=$_POST['showcase_description'];
$showcaseLink=$_POST['showcase_link'];
$showcaseUpload=$_POST['showcase_upload'];
$showcaseDisabled=$_POST['showcase_disabled'];
$administratorID=$_POST['administrator_ID'];

$update = "UPDATE showcase SET 
showcase_title      ='" . $showcaseTitle      . "', 
showcase_description      ='" . $showcaseDescription      . "', 
showcase_link      ='" . $showcaseLink      . "', 
showcase_upload      ='" . $showcaseUpload      . "', 
showcase_disabled      ='" . $showcaseDisabled      . "', 
administrator_ID  ='" . $administratorID  . "' 
WHERE showcase_ID  ='" . $showcaseID        . "'";

if ($mysqli->query($update) === TRUE) {
    include "../commands/sqlclose.php";
    header("Location: ../../admin/?showcase=" . $showcaseID);
} else {
    echo "Error updating record: " . $mysqli->error;
    include "../commands/sqlclose.php";
}

?>