        </div> <!-- CLOSING CONTENT -->
    </div> <!-- CLOSING CONTAINER -->

    <!-- CLOSE SQL CONNECTION -->
    <?php include "../common/commands/sqlclose.php"; ?>
    <!-- JQUERY -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- BOOTSTRAP -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <!-- CUSTOM JS -->
    <script src="../common/js/interface.js"></script>

    </body> <!-- CLOSING BODY -->
</html> <!-- CLOSING HTML -->