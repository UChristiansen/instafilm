<?php
// INCLUDE HEADER
include("layout/header.php");

// SET DEFAULT PAGE
$page = "dashboard"; 

// CONTROL WHAT PAGE DISPLAYS. GET FROM URL
if (isset($_GET['customer'])) {
    include("pages/kunder_detail.php");
} elseif (isset($_GET['order'])) {
    include("pages/bestillinger_detail.php");
} elseif (isset($_GET['showcase'])) {
    if ($_GET['showcase'] == "new") {
        include("pages/referencer_new.php");
    } else {
       include("pages/referencer_detail.php");     
    }
} else {
    if(isset($_GET['page']))  {
        $page = $_GET['page'];
    }
    include("pages/" . $page . ".php");
}

// INCLUDE FOOTER
include("layout/footer.php");
?>