<div class="row">
    <div class="col-xs-12 device-scroll">
        <h2>Aktive referencer</h2>
        <?php
        // Attempt select query execution
        $select = "
        SELECT showcase.showcase_ID, showcase.showcase_title, 
        administrators.administrator_name
        FROM showcase
        INNER JOIN administrators ON showcase.administrator_ID = administrators.administrator_ID
        WHERE showcase_disabled = '0';
        ";
        if($result = $mysqli->query($select)) /* RUN QUERY */ {
            if($result->num_rows > 0){ /* SHOW ONLY IF RESULT IS NOT EMPTY */ ?>
                <table class="table">
                    <thead>
                        <tr>
                            <th>Nr.</th>
                            <th>Titel</th>
                            <th>Lagt op af</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php while($row = $result->fetch_array()){ /* SHOW ARRAY */ ?>
                        <form action="?showcase=<?php echo $row['showcase_ID'] ?>" method="post">
                            <tr>
                                <td><?php echo $row['showcase_ID'] ?></td>
                                <td><?php echo $row['showcase_title'] ?></td>
                                <td><?php echo $row['administrator_name'] ?></td>
                                <td class="options">
                                    <input type="hidden" name="showcase_ID" value="<?php echo $row['showcase_ID']; ?>">
                                    <button type="submit" class="primary-option">Vis detaljer <i class='fa fa-chevron-right'></i></button>
                                    <button type="submit" class="delete" formaction="../common/commands/referencer_deactivate.php"><i class='fa fa-times' aria-hidden='true'></i></button>
                                </td>
                            </tr>
                        </form>
                    <?php } ?>
                    </tbody>
                </table>
                <?php 
                $result->free();
            } else{
                echo "Der er ingen aktive referencer.";
            }
        } else{
            echo "ERROR: Could not able to execute $sql. " . $mysqli->error;
        }
        ?>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 spacer"></div>
</div>
<div class="row">
    <div class="col-xs-12 device-scroll">
        <h2>Slettede referencer</h2>
        <?php
        // Attempt select query execution
        $select = "
        SELECT showcase.showcase_ID, showcase.showcase_title, 
        administrators.administrator_name
        FROM showcase
        INNER JOIN administrators ON showcase.administrator_ID = administrators.administrator_ID
        WHERE showcase_disabled = '1';
        ";
        if($result = $mysqli->query($select)) /* RUN QUERY */ {
            if($result->num_rows > 0){ /* SHOW ONLY IF RESULT IS NOT EMPTY */ ?>
                <table class="table">
                    <thead>
                        <tr>
                            <th>Nr.</th>
                            <th>Titel</th>
                            <th>Lagt op af</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php while($row = $result->fetch_array()){ /* SHOW ARRAY */ ?>
                        <form action="../common/commands/referencer_restore.php" method="post">
                            <tr>
                                <td><?php echo $row['showcase_ID'] ?></td>
                                <td><?php echo $row['showcase_title'] ?></td>
                                <td><?php echo $row['administrator_name'] ?></td>
                                <td class="options">
                                    <input type="hidden" name="showcase_ID" value="<?php echo $row['showcase_ID']; ?>">
                                    <button type="submit" class="restore"><i class='fa fa-life-ring' aria-hidden='true'></i> Gendan</button>
                                </td>
                            </tr>
                        </form>
                    <?php } ?>
                    </tbody>
                </table>
                <?php 
                $result->free();
            } else{
                echo "Der er ingen slettede referencer.";
            }
        } else{
            echo "ERROR: Could not able to execute $sql. " . $mysqli->error;
        }
        ?>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 spacer"></div>
</div>
<div class="row">
    <div class="col-xs-12">
        <a href="?showcase=new" class="btn btn-primary">Tilføj <i class='fa fa-life-plus' aria-hidden='true'></i></a>
    </div>
</div>