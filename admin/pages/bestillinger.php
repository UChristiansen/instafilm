<div class="row">
    <div class="col-xs-12 device-scroll">
        <h2>Aktive bestillinger</h2>
        <?php
        // Attempt select query execution
        $select = "
        SELECT orders.order_ID, orders.order_disabled, orders.order_time, orders.order_city, customers.customer_name, customers.customer_email
        FROM orders
        INNER JOIN customers ON orders.customer_ID = customers.customer_ID
        WHERE order_disabled = '0';
        ";
        if($result = $mysqli->query($select)) /* RUN QUERY */ {
            if($result->num_rows > 0){ /* SHOW ONLY IF RESULT IS NOT EMPTY */ ?>
                <table class="table">
                    <thead>
                        <tr>
                            <th>Nr.</th>
                            <th>Navn</th>
                            <th>E-mail</th>
                            <th>Tidspunkt</th>
                            <th>Sted</th>
                            <th class="options"></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php while($row = $result->fetch_array()){ /* SHOW ARRAY */ ?>
                        <form action="?order=<?php echo $row['order_ID'] ?>" method="post">
                            <tr>
                                <td><?php echo $row['order_ID'] ?></td>
                                <td><?php echo $row['customer_name'] ?></td>
                                <td><?php echo $row['customer_email'] ?></td>
                                <td><?php echo $row['order_time'] ?></td>
                                <td><?php echo $row['order_city'] ?></td>
                                <td class="options">
                                    <input type="hidden" name="order_ID" value="<?php echo $row['order_ID']; ?>">
                                    <button type="submit" class="primary-option">Vis detaljer <i class='fa fa-chevron-right'></i></button>
                                    <button type="submit" class="delete" formaction="../common/commands/bestillinger_deactivate.php"><i class='fa fa-times' aria-hidden='true'></i></button>
                                </td>
                            </tr>
                        </form>
                    <?php } ?>
                    </tbody>
                </table>
                <?php 
                $result->free();
            } else{
                echo "Der er ingen aktive ordrer.";
            }
        } else{
            echo "ERROR: Could not able to execute" . $mysqli->error;
        }
        ?>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 spacer"></div>
</div>
<div class="row">
    <div class="col-xs-12 device-scroll">
        <h2>Annullerede bestillinger</h2>
        <?php
        // Attempt select query execution
        $select = "
        SELECT orders.order_ID, orders.order_disabled, orders.order_time, orders.order_city, customers.customer_name, customers.customer_email
        FROM orders
        INNER JOIN customers ON orders.customer_ID = customers.customer_ID
        WHERE order_disabled = '1';
        ";
        if($result = $mysqli->query($select)) /* RUN QUERY */ {
            if($result->num_rows > 0){ /* SHOW ONLY IF RESULT IS NOT EMPTY */ ?>
                <table class="table">
                    <thead>
                        <tr>
                            <th>Nr.</th>
                            <th>Navn</th>
                            <th>Tidspunkt</th>
                            <th class="options"></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php while($row = $result->fetch_array()){ /* SHOW ARRAY */ ?>
                        <form action="../common/commands/bestillinger_restore.php" method="post">
                            <tr>
                                <td><?php echo $row['order_ID'] ?></td>
                                <td><?php echo $row['customer_name'] ?></td>
                                <td><?php echo $row['order_time'] ?></td>
                                <td class="options">
                                    <input type="hidden" name="order_ID" value="<?php echo $row['order_ID']; ?>">
                                    <button type="submit" class="restore"><i class='fa fa-life-ring' aria-hidden='true'></i> Gendan</button>
                                </td>
                            </tr>
                        </form>
                    <?php } ?>
                    </tbody>
                </table>
                <?php 
                $result->free();
            } else{
                echo "Der er ingen aktive ordrer.";
            }
        } else{
            echo "ERROR: Could not able to execute" . $mysqli->error;
        }
        ?>
    </div>
</div>