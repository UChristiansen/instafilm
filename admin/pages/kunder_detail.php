<?php     
$customerID = $_GET['customer']; /* SET ID FROM URL */
// Attempt select query execution
$select = "SELECT * FROM customers WHERE customer_ID = '" . $customerID . "'";
$result = $mysqli->query($select); /* RUN QUERY */

while ($row = $result->fetch_assoc()) { /* GET ROW */ ?>
<form action="../common/commands/kunder_update.php" method="post">
    <div class="row">
        <div class="col-xs-5 col-md-3 col-lg-2">    
            <div class="form-group">
                <input type="text" class="form-control" name="customer_ID" value="<?php echo $row['customer_ID']; ?>" id="number" readonly>
                <label for="number">Nummer</label>
            </div>
        </div>
        <div class="col-xs-5 col-md-3 col-lg-2 col-xs-offset-2 col-md-offset-6 col-lg-offset-8">    
            <div class="form-group">
                
                <div class="form-group">
                    <select class="form-control" name="customer_disabled" id="status">
                        <?php if($row['customer_disabled'] == false) {
                            echo "<option value='0' selected>Aktiv</option>";
                            echo "<option value='1'>Deaktiveret</option>";
                        } else {
                            echo "<option value='0'>Aktiv</option>";
                            echo "<option value='1' selected>Deaktiveret</option>";
                        } ?>
                    </select>
                    <label for="status">Status</label>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-lg-6">
            <div class="form-group">
                <input type="text" class="form-control" name="customer_name" value="<?php echo $row['customer_name']; ?>" id="name">
                <label for="name">Navn</label>
            </div>
        </div>
        <div class="col-xs-12 col-lg-6">
            <div class="form-group">
                <input type="text" class="form-control" name="customer_email" value="<?php echo $row['customer_email']; ?>" id="email">
                <label for="email">Email</label>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-lg-6">
            <div class="form-group">
                <input type="text" class="form-control" name="customer_address" value="<?php echo $row['customer_address']; ?>" id="address">
                <label for="address">Adresse</label>
            </div>
        </div>
        <div class="col-xs-4 col-lg-2">
            <div class="form-group">
                <input type="text" class="form-control" name="customer_zip" value="<?php echo $row['customer_zip']; ?>" id="zip">
                <label for="zip">Postnr.</label>
            </div>
        </div>
        <div class="col-xs-8 col-lg-4">
            <div class="form-group">
                <input type="text" class="form-control" name="customer_city" value="<?php echo $row['customer_city']; ?>" id="city">
                <label for="city">City</label>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-lg-6">
            <div class="form-group">
                <input type="number" class="form-control" name="customer_phone" value="<?php echo $row['customer_phone']; ?>" id="phone">
                <label for="phone">Telefonnr.</label>
            </div>
        </div>
        <div class="col-xs-12 col-lg-6">
            <div class="form-group">
                <input type="password" class="form-control" name="customer_password" value="<?php echo $row['customer_password']; ?>" id="password">
                <label for="password">Password</label>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 spacer"></div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-lg-6">
            <div class="form-group">
                <input type="text" class="form-control" name="customer_company" value="<?php echo $row['customer_company']; ?>" id="company">
                <label for="company">Firmanavn</label>
            </div>
        </div>
        <div class="col-xs-12 col-lg-6">
            <div class="form-group">
                <input type="text" class="form-control" name="customer_cvr" value="<?php echo $row['customer_cvr']; ?>" id="cvr">
                <label for="cvr">Cvr-nr.</label>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                <button type="submit" class="btn btn-danger" formaction="../common/commands/kunder_deactivate.php">Deaktivér konto</button>
            </div>
        </div>
        <div class="col-xs-6 text-right">
            <div class="form-group">
                <button type="submit" class="btn btn-success" id="save"><i class='fa fa-floppy-o'></i> Gem</button>
            </div>
        </div>
    </div>
</form>
<?php
}
?>