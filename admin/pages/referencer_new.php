<?php     
$select = "SELECT * FROM showcase";
$result = $mysqli->query($select);
$rowCount = mysqli_num_rows($result);

?>
<form action="../common/commands/referencer_insert.php" method="post">
    <div class="row">
        <div class="col-xs-5 col-md-3 col-lg-2">    
            <div class="form-group">
                <input type="text" class="form-control" name="showcase_ID" value="<?php echo ++$rowCount; ?>" id="number" readonly>
                <label for="number">Nummer</label>
            </div>
        </div>
        <div class="col-xs-5 col-md-3 col-lg-2 col-xs-offset-2 col-md-offset-6 col-lg-offset-8">    
            <div class="form-group">
                <select class="form-control" name="showcase_disabled" id="status">
                    <option value='0' selected>Aktiv</option>
                    <option value='1'>Deaktiveret</option>
                </select>
                <label for="status">Status</label>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="form-group">
                <input type="text" class="form-control" name="showcase_title" id="title">
                <label for="name">Titel</label>
                <input type="text" class="form-control" name="showcase_link" id="link">
                <label for="email">Link</label>
                <textarea id="description" class="form-control" name="showcase_description" id="description"></textarea>
                <label for="description">Beskrivelse</label>
                <input type="text" class="form-control" name="administrator_ID" id="administrator_ID">
                <label for="email">Administrator ID</label>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 text-right">
            <div class="form-group">
                <button type="submit" class="btn btn-success" id="save"><i class='fa fa-floppy-o'></i> Gem</button>
            </div>
        </div>
    </div>
</form>