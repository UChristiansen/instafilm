<div class="row">
    <div class="col-xs-12 device-scroll">
        <h2>Aktive kunder</h2>
        <?php
        // Attempt select query execution
        $select = "SELECT * FROM customers WHERE customer_disabled = '0'";
        if($result = $mysqli->query($select)) /* RUN QUERY */ {
            if($result->num_rows > 0){ /* SHOW ONLY IF RESULT IS NOT EMPTY */ ?>
                <table class="table">
                    <thead>
                        <tr>
                            <th>Nr.</th>
                            <th>Navn</th>
                            <th>E-mail</th>
                            <th>Aktivitet</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php while($row = $result->fetch_array()){ /* SHOW ARRAY */ ?>
                        <form action="?customer=<?php echo $row['customer_ID'] ?>" method="post">
                            <tr>
                                <td><?php echo $row['customer_ID'] ?></td>
                                <td><?php echo $row['customer_name'] ?></td>
                                <td><?php echo $row['customer_email'] ?></td>
                                <td><?php echo $row['customer_last-active'] ?></td>
                                <td class="options">
                                    <input type="hidden" name="customer_ID" value="<?php echo $row['customer_ID']; ?>">
                                    <button type="submit" class="primary-option">Vis detaljer <i class='fa fa-chevron-right'></i></button>
                                    <button type="submit" class="delete" formaction="../common/commands/kunder_deactivate.php"><i class='fa fa-times' aria-hidden='true'></i></button>
                                </td>
                            </tr>
                        </form>
                    <?php } ?>
                    </tbody>
                </table>
                <?php 
                $result->free();
            } else{
                echo "Der er ingen aktive kunder.";
            }
        } else{
            echo "ERROR: Could not able to execute $sql. " . $mysqli->error;
        }
        ?>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 spacer"></div>
</div>
<div class="row">
    <div class="col-xs-12 device-scroll">
        <h2>Deaktiverede kunder</h2>
        <?php
        // Attempt select query execution
        $select = "SELECT * FROM customers WHERE customer_disabled = '1'";
        if($result = $mysqli->query($select)) /* RUN QUERY */ {
            if($result->num_rows > 0){ /* SHOW ONLY IF RESULT IS NOT EMPTY */ ?>
                <table class="table">
                    <thead>
                        <tr>
                            <th>Nr.</th>
                            <th>Navn</th>
                            <th>E-mail</th>
                            <th>Aktivitet</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php while($row = $result->fetch_array()){ /* SHOW ARRAY */ ?>
                        <form action="../common/commands/kunder_restore.php" method="post">
                            <tr>
                                <td><?php echo $row['customer_ID'] ?></td>
                                <td><?php echo $row['customer_name'] ?></td>
                                <td><?php echo $row['customer_email'] ?></td>
                                <td><?php echo $row['customer_last-active'] ?></td>
                                <td class="options">
                                    <input type="hidden" name="customer_ID" value="<?php echo $row['customer_ID']; ?>">
                                    <button type="submit" class="restore"><i class='fa fa-life-ring' aria-hidden='true'></i> Gendan</button>
                                </td>
                            </tr>
                        </form>
                    <?php } ?>
                    </tbody>
                </table>
                <?php 
                $result->free();
            } else{
                echo "Der er ingen slettede kunder.";
            }
        } else{
            echo "ERROR: Could not able to execute $sql. " . $mysqli->error;
        }
        ?>
    </div>
</div>