<div class="row">
    <div class="col-xs-12 col-lg-6">
        <h2>Velkommen NAVN!</h2>
        <p>Velkommen til administrationspanelet for Instafilm!</p>
    </div>
    <div class="col-xs-12 col-lg-6">
        <h2>Nyeste bestillinger</h2>
        <?php
            // Attempt select query execution
            $select = "
            SELECT orders.order_ID, orders.order_time, customers.customer_name
            FROM orders
            INNER JOIN customers ON orders.customer_ID = customers.customer_ID
            WHERE order_disabled = '0';
            ";
            if($result = $mysqli->query($select)) /* RUN QUERY */ {
                if($result->num_rows > 0){ /* SHOW ONLY IF RESULT IS NOT EMPTY */ ?>
            <table class="table">
                    <thead>
                        <tr>
                            <th>Navn</th>
                            <th>Tidspunkt</th>
                            <th class="options"></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php while($row = $result->fetch_array()){ /* SHOW ARRAY */ ?>
                        <form action="?order=<?php echo $row['order_ID'] ?>" method="post">
                            <tr>
                                <td><?php echo $row['customer_name'] ?></td>
                                <td><?php echo $row['order_time'] ?></td>
                                <td class="options">
                                    <input type="hidden" name="order_ID" value="<?php echo $row['order_ID']; ?>">
                                    <button type="submit" class="primary-option">Vis detaljer <i class='fa fa-chevron-right'></i></button>
                                </td>
                            </tr>
                        </form>
                    <?php } ?>
                    </tbody>
                </table>
            <?php 
                $result->free();
            } else{
                echo "Der er ingen aktive ordrer.";
            }
        } else{
            echo "ERROR: Could not able to execute" . $mysqli->error;
        }
        ?>
        <div class="spacer"></div>
        <h2>Afbestillinger</h2>
        <table class="table">
            <thead>
                <tr>
                    <th>Navn</th>
                    <th>Tidspunkt</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Hans Jørgensen</td>
                    <td>20/07/17</td>
                    <td class="options">
                        <a href="#" class="accept"><i class="fa fa-check" aria-hidden="true"></i></a>
                        <a href="#" class="delete"><i class="fa fa-times" aria-hidden="true"></i></a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>