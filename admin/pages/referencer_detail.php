<?php     
$showcaseID = $_GET['showcase']; /* SET ID FROM URL */
// Attempt select query execution
$select = "
SELECT showcase.showcase_ID, showcase.showcase_title, showcase.showcase_description, showcase.showcase_link,  showcase.showcase_upload, showcase.showcase_disabled, 
administrators.administrator_ID
FROM showcase
INNER JOIN administrators ON showcase.administrator_ID = administrators.administrator_ID
WHERE showcase_ID = '" . $showcaseID . "';
";
$result = $mysqli->query($select); /* RUN QUERY */
while ($row = $result->fetch_assoc()) { /* GET ROW */ ?>
<form action="../common/commands/referencer_update.php" method="post">
    <div class="row">
        <div class="col-xs-5 col-md-3 col-lg-2">    
            <div class="form-group">
                <input type="text" class="form-control" name="showcase_ID" value="<?php echo $row['showcase_ID']; ?>" id="number" readonly>
                <label for="number">Nummer</label>
            </div>
        </div>
        <div class="col-xs-5 col-md-3 col-lg-2 col-xs-offset-2 col-md-offset-6 col-lg-offset-8">    
            <div class="form-group">
                  <select class="form-control" name="showcase_disabled" id="status">
                      <?php if($row['showcase_disabled'] == false) {
                          echo "<option value='0' selected>Aktiv</option>";
                          echo "<option value='1'>Deaktiveret</option>";
                      } else {
                          echo "<option value='0'>Aktiv</option>";
                          echo "<option value='1' selected>Deaktiveret</option>";
                      } ?>
                  </select>
                  <label for="status">Status</label>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-lg-4">
            <!-- DISPLAY VIDEO -->
            <iframe id="ytplayer" type="text/html" src="https://www.youtube.com/embed/<?php echo $row['showcase_link']; ?>"
              frameborder="0"></iframe>
            <!-- FACEBOOK SHARE -->
            <div class="fb-share-button" data-href="https://www.youtube.com/watch?v=<?php echo $row['showcase_link']; ?>" data-layout="button_count" data-size="large" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3D<?php echo $row['showcase_link']; ?>&amp;src=sdkpreparse">Del</a></div>
        </div>
        <div class="col-xs-12 col-lg-8">
            <div class="form-group">
                <input type="text" class="form-control" name="showcase_title" value="<?php echo $row['showcase_title']; ?>" id="name">
                <label for="name">Titel</label>
                <input type="text" class="form-control" name="showcase_upload" value="<?php echo $row['showcase_upload']; ?>" id="upload">
                <label for="email">Uploaded</label>
                <input type="text" class="form-control" name="showcase_link" value="<?php echo $row['showcase_link']; ?>" id="link">
                <label for="email">Link</label>
                <textarea id="description" class="form-control" name="showcase_description"><?php echo $row['showcase_description']; ?></textarea>
                <label for="description">Beskrivelse</label>
                <input type="text" class="form-control" name="administrator_ID" value="<?php echo $row['administrator_ID']; ?>" id="administrator_ID">
                <label for="email">Administrator ID</label>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                <!-- DELETE BUTTON -->
                <button type="submit" class="btn btn-danger" formaction="../common/commands/referencer_deactivate.php">Slet reference</button>
            </div>
        </div>
        <div class="col-xs-6 text-right">
            <div class="form-group">
                <!-- SAVE VIDEO -->
                <button type="submit" class="btn btn-success" id="save"><i class='fa fa-floppy-o'></i> Gem</button>
            </div>
        </div>
    </div>
</form>
<?php
}
?>