<div class="row">
    <div class="col-xs-12 device-scroll">
        <?php
        // Attempt select query execution
        $select = "SELECT * FROM administrators";
        if($result = $mysqli->query($select)) /* RUN QUERY */ {
            if($result->num_rows > 0){ /* SHOW ONLY IF RESULT IS NOT EMPTY */ ?>
                <table class="table">
                    <thead>
                        <tr>
                            <th>Nr.</th>
                            <th>Navn</th>
                            <th>E-mail</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php while($row = $result->fetch_array()){ /* SHOW ARRAY */ ?>
                        <form action="../common/commands/administratorer_delete.php" method="post">
                            <tr>
                                <td><?php echo $row['administrator_ID'] ?></td>
                                <td><?php echo $row['administrator_name'] ?></td>
                                <td><?php echo $row['administrator_email'] ?></td>
                                <td class="options">
                                    <input type="hidden" name="administrator_ID" value="<?php echo $row['administrator_ID']; ?>">
                                    <button type="submit" class="secondary-option"  formaction="../common/commands/change_password.php">Ændre kodeord</button>
                                    <button type="submit" class="delete"><i class='fa fa-times' aria-hidden='true'></i></button>
                                </td>
                            </tr>
                        </form>
                    <?php } ?>
                    </tbody>
                </table>
                <?php 
                $result->free();
            } else{
                echo "Der er ingen administratorer.";
            }
        } else{
            echo "ERROR: Could not able to execute $sql. " . $mysqli->error;
        }
        ?>
    </div>
</div>