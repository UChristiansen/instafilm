<?php     
$orderID = $_GET['order']; /* SET ID FROM URL */
// Attempt select query execution
$select = "
SELECT orders.order_ID, orders.order_disabled, orders.order_theme, orders.order_purpose, orders.order_targetgroup, orders.order_time, orders.order_address, orders.order_zip, orders.order_city, orders.order_addons,
customers.customer_name, customers.customer_email, customers.customer_phone, customers.customer_address, customers.customer_zip, customers.customer_city, customers.customer_company, customers.customer_cvr
FROM orders
INNER JOIN customers ON orders.customer_ID = customers.customer_ID
WHERE order_ID = '" . $orderID . "';
";
$result = $mysqli->query($select); /* RUN QUERY */

while ($row = $result->fetch_assoc()) { /* GET ROW */ ?> 
<div class="row">
    <div class="col-xs-12">
        <h2>Bestillingsoplysninger</h2>
        <table class="table table-strict-first">
            <thead>
                <tr>
                    <th>Attribut</th>
                    <th>Detalje</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Nummer</td>
                    <td><?php echo $row['order_ID']; ?></td>
                </tr>
                <tr>
                    <td>Status</td>
                    <td>
                        <?php if($row['order_disabled'] == false) {
                            echo "Aktiv";
                        } else {
                            echo "Annulleret";
                        } ?>
                    
                    </td>
                </tr>
                <tr>
                    <td>Tema</td>
                    <td><?php echo $row['order_theme']; ?></td>
                </tr>
                <tr>
                    <td>Formål</td>
                    <td><?php echo $row['order_purpose']; ?></td>
                </tr>
                <tr>
                    <td>Målgruppe</td>
                    <td><?php echo $row['order_targetgroup']; ?></td>
                </tr>
                <tr>
                    <td>Tidspunkt</td>
                    <td><?php echo $row['order_time']; ?></td>
                </tr>
                <tr>
                    <td>Sted</td>
                    <td><?php echo $row['order_address']; ?><br><?php echo $row['order_zip']; ?> <?php echo $row['order_city']; ?></td>
                </tr>
                <tr>
                    <td>Tilkøb</td>
                    <td><?php echo $row['order_addons']; ?></td>
                </tr>
            </tbody>
        </table>
        <div class="spacer"></div>
        <h2>Faktureringsoplysninger</h2>
        <table class="table table-strict-first">
            <thead>
                <tr>
                    <th>Attribut</th>
                    <th>Detalje</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Navn</td>
                    <td><?php echo $row['customer_name']; ?></td>
                </tr>
                <tr>
                    <td>E-mail</td>
                    <td><?php echo $row['customer_email']; ?></td>
                </tr>
                <tr>
                    <td>Telefonnummer</td>
                    <td><?php echo $row['customer_phone']; ?></td>
                </tr>
                <tr>
                    <td>Adresse</td>
                    <td><?php echo $row['customer_address']; ?></td>
                </tr>
                <tr>
                    <td>Postnummer</td>
                    <td><?php echo $row['customer_zip']; ?></td>
                </tr>
                <tr>
                    <td>By</td>
                    <td><?php echo $row['customer_city']; ?></td>
                </tr>
                <tr>
                    <td>Firmanavn</td>
                    <td><?php echo $row['customer_company']; ?></td>
                </tr>
                <tr>
                    <td>CVR-nummer</td>
                    <td><?php echo $row['customer_cvr']; ?></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<?php
}
?>
<div class="row">
    <div class="col-xs-12 spacer"></div>
</div>
<div class="row">
    <div class="col-xs-12 text-center">
        <a href="?page=bestillinger" class="btn btn-primary"><i class="fa fa-chevron-left" aria-hidden="true"></i> Tilbage til bestillinger</a>
    </div>
</div>