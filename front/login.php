<?php
    // INCLUDE HEADER
    include("layout/header_login.php");
    ?>


    <div class="container">
        <div class="row">
            <div id="login-box" class="col-xs-12 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                <img src="../common/media/logo_color.png" class="logo">
                <form id="loginform">
                    <div class="form-group">
                        <input type="text" name="brugernavn" class="form-control" id="brugernavn" placeholder="">
                        <label for="brugernavn">Brugernavn</label>
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" class="form-control" id="password" placeholder="">
                        <label for="password">Password</label>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <a href="createaccount.php" class="btn btn-default">Opret bruger</a>
                        </div>
                        <div class="col-xs-6 text-right">
                            <button type="submit" class="btn btn-primary float-right">Log ind <i class="fa fa-unlock" aria-hidden="true"></i></button>
                        </div>
                    </div>
                </form>
                <div class="row">
                    <div class="col-xs-12 spacer"></div>
                </div>
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <a class="btn btn-link" href="index.php"><i class="fa fa-long-arrow-left" aria-hidden="true"></i> Tilbage til forsiden</a>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <?php
    // INCLUDE FOOTER
    include("layout/footer.php");
    ?>