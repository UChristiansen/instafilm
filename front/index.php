<?php
    // INCLUDE HEADER
    include("layout/header.php");
    ?>

    <section id="banner">
        <video autoplay loop muted>
            <source src="../common/media/introfilm.mp4" type="video/mp4">
            Your browser does not support the video tag.
        </video>
        <button class="btn play-sound"><i class="fa fa-volume-up" aria-hidden="true"></i> Spil med lyd</button>
        <div class="order-panel">
            <p class="lead">Lorem ipsum</p>
            <button class="btn btn-primary btn-sticker">Bestil film</button>
        </div>
    </section>

    <section id="social-bar">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-lg-3">
                    <a href="https://www.facebook.com/Instafilm/?fref=ts"><i class="fa fa-facebook-official fa-fw" aria-hidden="true"></i> Find os Facebook</a>
                </div>
                <div class="col-xs-12 col-sm-6 col-lg-3">
                    <a href="https://www.youtube.com/channel/UCOCM0vIe161v60L_mlKqweQ"><i class="fa fa-youtube-play fa-fw" aria-hidden="true"></i> Abonnér på Youtube</a>
                </div>
                <div class="col-xs-12 col-sm-6 col-lg-3">
                    <a href="https://www.linkedin.com/company-beta/5026282/"><i class="fa fa-linkedin-square fa-fw" aria-hidden="true"></i> Find os på LinkedIn</a>
                </div>
                <div class="col-xs-12 col-sm-6 col-lg-3">
                    <a href="https://www.instagram.com/instafilmofficial/"><i class="fa fa-instagram fa-fw" aria-hidden="true"></i> Følg os på Instagram</a>
                </div>
            </div>
        </div>
    </section>
    <section id="genres">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-lg-4 text-center">
                    <i class="fa fa-facebook-official fa-3x"></i>
                    <h2>Facebook</h2>
                    <p>Cuptaestotat exerum facia volum rempora tiossit ionsed mossequaecto eliquia temque porum est fugiame maximpo ratessimus sandero vitentur re pelest, comnihicabo. Itatis maionseque net, to offici doluptati doluptincti sinimodipsam accumque con percia voluptisi dus accat rerias exero conseri dendani molestias am harchilit am exerorunt es.</p>
                    <button type="button" class="btn btn-primary">Læs mere</button>
                </div>
                <div class="col-xs-12 col-lg-4 text-center">
                    <i class="fa fa-bullhorn fa-3x"></i>
                    <h2>Viral</h2>
                    <p>Cuptaestotat exerum facia volum rempora tiossit ionsed mossequaecto eliquia temque porum est fugiame maximpo ratessimus sandero vitentur re pelest, comnihicabo. Itatis maionseque net, to offici doluptati doluptincti sinimodipsam accumque con percia voluptisi dus accat rerias exero conseri dendani molestias am harchilit am exerorunt es.</p>
                    <button type="button" class="btn btn-primary">Læs mere</button>
                </div>
                <div class="col-xs-12 col-lg-4 text-center">
                    <i class="fa fa-bullseye fa-3x"></i>
                    <h2>Nå ud til målgruppen</h2>
                    <p>Cuptaestotat exerum facia volum rempora tiossit ionsed mossequaecto eliquia temque porum est fugiame maximpo ratessimus sandero vitentur re pelest, comnihicabo. Itatis maionseque net, to offici doluptati doluptincti sinimodipsam accumque con percia voluptisi dus accat rerias exero conseri dendani molestias am harchilit am exerorunt es.</p>
                    <button type="button" class="btn btn-primary">Læs mere</button>
                </div>
            </div>
        </div>
    </section>
    <section id="video-teaser">
        <div class="container">
            <div class="row d-flex justify-content-center">
                <div class="col-xs-12 col-lg-8 video-image">
                    <img src="../common/media/instafilm_thumb.jpg" class="img-responsive">
                </div>
                <div class="col-xs-12 col-lg-4 video-text">
                    <h2>Fra os til jer</h2>
                    <h1>Instafilm</h1>
                    <p>Cuptaestotat exerum facia volum rempora tiossit ionsed mossequaecto eliquia temque porum est fugiame maximpo ratessimus sandero vitentur re pelest, comnihicabo. Itatis maionseque net, to offici doluptati doluptincti sinimodipsam accumque con percia voluptisi dus accat rerias exero conseri dendani molestias am harchilit am exerorunt es nonsequas ut lissiti buscilitae velesed mo eatiatibusa cumquam.</p>
                    <button type="button" class="btn btn-primary">Om os</button>
                    <button type="button" class="btn btn-default">Kontakt</button>
                </div>
            </div>
        </div>
    </section>
    <section id="showcase-teaser">
        <div class="container">
            <div class="row text-center">
                <div class="col-12">
                    <h1>Referencer</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-6">
                    <div class="row">
                        <div class="col-xs-4 col-lg-6 showcase-image">
                            <img src="../common/media/instafilm_thumb2.png" class="img-responsive">
                        </div>
                        <div class="col-xs-8 col-lg-6 showcase-text">
                            <h2>Rubrik 1</h2>
                            <p>Cuptaestotat exerum facia volum rempora tiossit ionsed mossequaecto eliquia temque porum est fugiame maximpo ratessimus sandero vitentur re</p>
                            <div class="row">
                                <div class="col-xs-6">
                                    <p class="time">01:08</p>
                                </div>
                                <div class="col-xs-6">
                                    <p class="date">32/01/17</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6">
                    <div class="row">
                        <div class="col-xs-4 col-lg-6 showcase-image">
                            <img src="../common/media/instafilm_thumb2.png" class="img-responsive">
                        </div>
                        <div class="col-xs-8 col-lg-6 showcase-text">
                            <h2>Rubrik 1</h2>
                            <p>Cuptaestotat exerum facia volum rempora tiossit ionsed mossequaecto eliquia temque porum est fugiame maximpo ratessimus sandero vitentur re</p>
                            <div class="row">
                                <div class="col-xs-6">
                                    <p class="time">01:08</p>
                                </div>
                                <div class="col-xs-6">
                                    <p class="date">32/01/17</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row text-center">
                <div class="col-12">
                    <button type="button" class="btn btn-primary mb-5">Se alle referencer</button>
                </div>
            </div>
        </div>
    </section>

    <section id="testimonials">
        <div class="container">

            <h5 style="text-align: center; margin-top: 40px;">Det fortæller de</h5>

            <h2 style="text-align: center; color: #6b1c99; padding-bottom: 80px;">Anmeldelser</h2>

            <div id="reviews" class="row">
                <div class="col-md-4 pb-3 review" style="background-color: #f0f0f0; border-left: 15px solid white; border-right: 5px solid white;">
                    <img src="../common/media/apostrophe.png" class="pb-3 pt-3" style="width: 30px;"><img src="../common/media/circle.png" style="width: 100px; float: right; margin: -50px 20px 0 0;">
                    <h5>Firma</h5>
                    <h4 style="color: #6b1c99;">Ken Hansen</h4>
                    <p>Cuptaestotat exerum facia volum rempora tiossit ionsed mossequaecto eliquia temque porum est fugiame maximpo ratessimus sandero vitentur re pelest, comnihicabo. Itatis maionseque net, to offici doluptati doluptincti sinimodipsam accumque con percia voluptisi dus accat rerias exero conseri dendani molestias am harchilit am exerorunt es.</p>
                </div>
                <div class="col-md-4 pb-3 review" style="background-color: #f0f0f0; border-right: 10px solid white; border-left: 10px solid white;">
                    <img src="../common/media/apostrophe.png" class="pb-3 pt-3" style="width: 30px;"><img src="../common/media/circle.png" style="width: 100px; float: right; margin: -50px 20px 0 0;">
                    <h5>Firma</h5>
                    <h4 style="color: #6b1c99;">Ken Hansen</h4>
                    <p>Cuptaestotat exerum facia volum rempora tiossit ionsed mossequaecto eliquia temque porum est fugiame maximpo ratessimus sandero vitentur re pelest, comnihicabo. Itatis maionseque net, to offici doluptati doluptincti sinimodipsam accumque con percia voluptisi dus accat rerias exero conseri dendani molestias am harchilit am exerorunt es.</p>
                </div>
                <div class="col-md-4 pb-3 review" style="background-color: #f0f0f0; border-right: 15px solid white; border-left: 5px solid white;">
                    <img src="../common/media/apostrophe.png" class="pb-3 pt-3" style="width: 30px;"><img src="../common/media/circle.png" style="width: 100px; float: right; margin: -50px 20px 0 0;">
                    <h5>Firma</h5>
                    <h4 style="color: #6b1c99;">Ken Hansen</h4>
                    <p>Cuptaestotat exerum facia volum rempora tiossit ionsed mossequaecto eliquia temque porum est fugiame maximpo ratessimus sandero vitentur re pelest, comnihicabo. Itatis maionseque net, to offici doluptati doluptincti sinimodipsam accumque con percia voluptisi dus accat rerias exero conseri dendani molestias am harchilit am exerorunt es.</p>
                </div>
            </div>

            <div class="col-sm-12" style="text-align: center; margin: 30px 0 30px 0;">
                <button type="button" class="btn btn-primary mb-5">
                        SE ALLE Anmeldelser</button>
            </div>
        </div>
    </section>

    <section id="footer">
        <div class="container">
            <p></p>
            <h2></h2>
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-lg-4">
                    <img class="logo" src="../common/media/logo_negative.png">
                    <ul>
                        <li><i class="fa fa-map-marker fa-fw" aria-hidden="true"></i> Børge Jensens Plads 1, 1. Sal <span>5800 Nyborg</span></li>
                        <li><i class="fa fa-phone fa-fw" aria-hidden="true"></i> +45 6075 3489</li>
                        <li><i class="fa fa-envelope fa-fw" aria-hidden="true"></i> info@instafilm.dk</li>
                    </ul>
                </div>
                <div class="col-xs-12 col-sm-6 col-lg-3">
                    <h2>Social</h2>
                    <ul>
                        <li><a href="https://www.facebook.com/Instafilm/?fref=ts"><i class="fa fa-facebook-official fa-fw" aria-hidden="true"></i> Find os på Facebook</a></li>
                        <li><a href="https://www.youtube.com/channel/UCOCM0vIe161v60L_mlKqweQ"><i class="fa fa-youtube-play fa-fw" aria-hidden="true"></i> Abonnér på Youtube</a></li>
                        <li><a href="https://www.linkedin.com/company-beta/5026282/"><i class="fa fa-linkedin-square fa-fw" aria-hidden="true"></i> Find os på LinkedIn</a></li>
                        <li><a href="https://www.instagram.com/instafilmofficial/"><i class="fa fa-instagram fa-fw" aria-hidden="true"></i> Find os på Instagram</a></li>
                    </ul>
                </div>
                <div class="col-xs-12 col-lg-5">
                    <h2>Nyhedsbrev</h2>
                    <p>Cuptaestotat exerum facia volum rempora tiossit ionsed mossequaecto eliquia temque porum est</p>
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Hvad er din mail?">
                        <span class="input-group-btn">
                            <button class="btn btn-primary" type="button">Tilmeld</button>
                        </span>
                    </div>
                </div>
            </div>



        </div>
    </section>





    <?php
    // INCLUDE FOOTER
    include("layout/footer.php");
    ?>