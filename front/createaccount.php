<?php
    // INCLUDE HEADER
    include("layout/header_login.php");
    ?>


    <div class="container">
        <div class="row">
            <div id="login-box" class="col-xs-12 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                <img src="../common/media/logo_color.png" class="logo">
                <form id="createaccount">
                    <div class="form-group">
                        <input type="text" class="form-control" id="navn" name="navn" placeholder="">
                        <label for="navn">Navn</label>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="brugernavn" name="brugernavn" placeholder="">
                        <label for="brugernavn">Brugernavn</label>
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" id="password" name="password" placeholder="">
                        <label for="password">Password</label>
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" id="passwordconf" name="passwordconf" placeholder="">
                        <label for="passwordconf">Gentag password</label>
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control" id="email" name="email" placeholder="">
                        <label for="email">Email</label>
                    </div>
                    <div class="form-group">
                        <input type="digits" class="form-control" id="mobilnummer" name="mobilnummer" placeholder="">
                        <label for="mobilnummer">Mobilnummer</label>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <a href="login.php" class="btn btn-default">Log ind <i class="fa fa-unlock" aria-hidden="true"></i></a>
                        </div>
                        <div class="col-xs-6 text-right">
                            <button type="submit" class="btn btn-primary float-right">Opret bruger</button>
                        </div>
                    </div>
                </form>
                <div class="row">
                    <div class="col-xs-12 spacer"></div>
                </div>
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <a class="btn btn-link" href="index.php"><i class="fa fa-long-arrow-left" aria-hidden="true"></i> Tilbage til forsiden</a>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <?php
    // INCLUDE FOOTER
    include("layout/footer.php");
    ?>