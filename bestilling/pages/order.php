<div class="row">
    <aside class="col-xs-12 col-lg-3" id="progress">
        <div class="row">
            <div class="col-xs-5 col-sm-6">
                <h2>Fremskridt</h2>
            </div>
            <div class="col-xs-5 col-sm-5 text-right">
                <h3>Start</h3>
            </div>
            <div class="col-xs-2 col-sm-1 text-right">
                <button type="button" id="progress-toggle"><i class="fa fa-chevron-down" aria-hidden="true"></i></button>
            </div>
        </div>
        <ul id="progress-items">
            <li class="active"><a href="#task1"><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Start</a></li>
            <li><a href="#task2"><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Tema</a></li>
            <li><a href="#task3"><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Formål</a></li>
            <li><a href="#task4"><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Målgruppe</a></li>
            <li><a href="#task5"><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Sted</a></li>
            <li><a href="#task6"><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Tidspunkt</a></li>
            <li><a href="#task7"><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Tilkøb</a></li>
            <li><a href="#task8"><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Kundeoplysninger</a></li>
        </ul>
    </aside>
    <article class="col-xs-12 col-lg-6" id="task-window">
        <h1 id="title">Bestilling</h1>
        <form action="../common/commands/bestillinger_insert.php" method="post" class="form-inline">
            <div class="tasks">
                <div id="task1" class="task active">
                    <h2>Start</h2>
                    <p>Velkomme tekst tekst!</p>
                    <div class="row back-forth-buttons">
                        <div class="col-xs-12 text-right">
                            <a href="#task2" class="btn btn-lg btn-primary task-forward">Næste <i class='fa fa-chevron-right'></i></a>
                        </div>
                    </div>
                </div>
                <div id="task2" class="task">
                    <h2>Tema</h2>
                    <p class="lead">Fortæl os hvordan din film skal være, og hvilke følelser den skal fremkalde hos modtageren.</p>
                    <div class="form-group">
                        <label for="email">Min film skal være </label>
                        <input type="text" class="form-control" name="order_theme" aria-describedby="tema-help">
                        <span id="tema-help" class="help-block">Eksempelvis: Informativ, kreativ, humoristisk, alvorlig. Kommaseparér gerne hvis flere temaer.</span>
                    </div>
                    <div class="row back-forth-buttons">
                        <div class="col-xs-6">
                            <a href="#task1" class="btn btn-lg btn-primary task-backward">Forrige <i class='fa fa-chevron-left'></i></a>
                        </div>
                        <div class="col-xs-6 text-right">
                            <a href="#task3" class="btn btn-lg btn-primary task-forward">Næste <i class='fa fa-chevron-right'></i></a>
                        </div>
                    </div>
                </div>
                <div id="task3" class="task">
                    <h2>Formål</h2>
                    <p class="lead">Beskriv med dine egne ord hvorfor filmen skal laves</p>
                    <div class="form-group">
                        <label for="email">Filmen skal produceres/laves i forbindelse med </label>
                        <input type="text" class="form-control" name="order_purpose">
                        <span id="tema-help" class="help-block">Eksempelvis: jubilæum, produktlancering eller underholdning til firmafest.</span>
                    </div>
                    <div class="row back-forth-buttons">
                        <div class="col-xs-6">
                            <a href="#task2" class="btn btn-lg btn-primary task-backward">Forrige <i class='fa fa-chevron-left'></i></a>
                        </div>
                        <div class="col-xs-6 text-right">
                            <a href="#task4" class="btn btn-lg btn-primary task-forward">Næste <i class='fa fa-chevron-right'></i></a>
                        </div>
                    </div>
                </div>
                <div id="task4" class="task">
                    <h2>Målgruppe</h2>
                    <p class="lead">Hvem henvender filmen sig til?</p>
                    <div class="form-group">
                        <label for="email">Min eller mine målgrupper er </label>
                        <input type="text" class="form-control" name="order_purpose">
                        <span id="tema-help" class="help-block">Eksempelvis: 20-35 årige, folk der arbejder med IT, singler i Jylland.</span>
                    </div>
                    <div class="row back-forth-buttons">
                        <div class="col-xs-6">
                            <a href="#task3" class="btn btn-lg btn-primary task-backward">Forrige <i class='fa fa-chevron-left'></i></a>
                        </div>
                        <div class="col-xs-6 text-right">
                            <a href="#task5" class="btn btn-lg btn-primary task-forward">Næste <i class='fa fa-chevron-right'></i></a>
                        </div>
                    </div>
                </div>
                <div id="task5" class="task">
                    <h2>Sted</h2>
                    <p class="lead">Hvor skal filmen skydes henne?</p>
                    <div class="form-group">
                        <label for="email">Adresse</label>
                        <input type="text" class="form-control" name="order_address">
                    </div>
                    <div class="form-group">
                        <label for="email">Postnummer</label>
                        <input type="text" class="form-control" name="order_zip">
                    </div>
                    <div class="form-group">
                        <label for="email">By</label>
                        <input type="text" class="form-control" name="order_city">
                    </div>
                    <div class="row back-forth-buttons">
                        <div class="col-xs-6">
                            <a href="#task4" class="btn btn-lg btn-primary task-backward">Forrige <i class='fa fa-chevron-left'></i></a>
                        </div>
                        <div class="col-xs-6 text-right">
                            <a href="#task6" class="btn btn-lg btn-primary task-forward">Næste <i class='fa fa-chevron-right'></i></a>
                        </div>
                    </div>
                </div>
                <div id="task6" class="task">
                    <h2>Tidspunkt</h2>
                    <p class="lead">Beskriv med dine egne ord hvorfor filmen skal laves (eks. jubilæum, produktlancering eller underholdning til firmafest)</p>
                    <div class="form-group">
                        <label for="email">Filmen skal produceres/laves i forbindelse med </label>
                        <input type="datetime-local" class="form-control" name="order_time">
                    </div>
                    <div class="row back-forth-buttons">
                        <div class="col-xs-6">
                            <a href="#task5" class="btn btn-lg btn-primary task-backward">Forrige <i class='fa fa-chevron-left'></i></a>
                        </div>
                        <div class="col-xs-6 text-right">
                            <a href="#task7" class="btn btn-lg btn-primary task-forward">Næste <i class='fa fa-chevron-right'></i></a>
                        </div>
                    </div>
                </div>

                <div id="task7" class="task">
                    <h2>Tilkøb</h2>
                    <p class="lead">Beskriv med dine egne ord hvorfor filmen skal laves (eks. jubilæum, produktlancering eller underholdning til firmafest)</p>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" value="Musik" name="order_addons" id="addons_music">
                            Musik
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" value="Undertekster" name="order_addons" id="addons_subtitles">
                            Undertekster
                        </label>
                    </div>
                    <div class="row back-forth-buttons">
                        <div class="col-xs-6">
                            <a href="#task6" class="btn btn-lg btn-primary task-backward">Forrige <i class='fa fa-chevron-left'></i></a>
                        </div>
                        <div class="col-xs-6 text-right">
                            <a href="#task8" class="btn btn-lg btn-primary task-forward">Næste <i class='fa fa-chevron-right'></i></a>
                        </div>
                    </div>
                </div>

                <div id="task8" class="task">
                    <h2>Faktureringsoplysninger</h2>
                    <p class="lead">Beskriv med dine egne ord hvorfor filmen skal laves (eks. jubilæum, produktlancering eller underholdning til firmafest)</p>
                    <div class="form-group">
                        <label for="email">Indtast kunde-ID </label>
                        <input type="text" class="form-control" name="customer_ID">
                    </div>
                    <div class="row back-forth-buttons">
                        <div class="col-xs-6">
                            <a href="#task7" class="btn btn-lg btn-primary task-backward">Forrige <i class='fa fa-chevron-left'></i></a>
                        </div>
                        <div class="col-xs-6 text-right">
                            <button type="submit" class="btn btn-lg btn-success">Send bestilling <i class='fa fa-chevron-right'></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </article>
    <aside class="col-xs-12 col-lg-3" id="cart">
        <div class="row">
            <div class="col-xs-5 col-sm-6">
                <h2>Kurv</h2>
            </div>
            <div class="col-xs-5 col-sm-5 text-right">
                <h3>I alt <span class="cart-total-price">3999 DKK</span></h3>
            </div>
            <div class="col-xs-2 col-sm-1 text-right">
                <button type="button" id="cart-toggle"><i class="fa fa-chevron-up" aria-hidden="true"></i></button>
            </div>
        </div>
        <table class="table" id="cart-items">
            <tbody>
                <tr id="cart-film">
                    <td class="options"></td>
                    <td>Film, grundpris</td>
                    <td>3999 DKK</td>
                </tr>
                <tr id="cart-total">
                    <td class="options"></td>
                    <td class="total">I alt (ekskl. moms)</td>
                    <td class="cart-total-price">3999 DKK</td>
                </tr>
            </tbody>
        </table>
    </aside>
</div>