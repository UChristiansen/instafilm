<?php
// INCLUDE HEADER
include("layout/header.php");

$page = "order";
if(isset($_GET['page']))  {
    $page = $_GET['page'];
}

include("pages/" . $page . ".php");

// INCLUDE FOOTER
include("layout/footer.php");
?>